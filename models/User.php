<?php

/**
* Class User generated by Chrysalis v.1.0.0
* 
* Chrysalis info:
* @author pabhoz
* github: @pabhoz
* bitbucket: @pabhoz
* 
*/ 
 
class User extends \Fox\FoxModel {

    private $id;
    private $name;
    private $password;
    private $email;

    public function __construct( $id,  string $name, string $password, string $email) {

        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->email = $email;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function setPassword(string $password) {
        $this->password = $password;
    }

    public function setEmail(string $email) {
        $this->email = $email;
    }

    public function getMyVars(){
 return get_object_vars($this);
    }

}