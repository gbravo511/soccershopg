<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>
    <?php print $this->title ?>
  </title>
  <link rel="stylesheet" href="<?php echo URL."public/styles/style.css "; ?>">
</head>

<body>
        
  <div class="container">
      <?php foreach ($this->players as $player) : ?>
        <div class="player">
            <img src="<?php echo URL."public/images/".$player["img"]; ?>" alt="">
            <div class="name"><?php print $player["name"]; ?></div>
        </div>
    <?php endforeach; ?>
  </div>


</body>

</html>
