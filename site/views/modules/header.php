<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title><?php print($this->title); ?></title>
        <link rel="stylesheet" href="<?php print(URL); ?>public/vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php print(URL); ?>public/styles/style.css"/>
        <?php echo $stylesheets; ?>
    </head>