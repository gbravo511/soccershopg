<script>
    function ajaxSubmit(event){
        event.preventDefault();
        form = event.target;
        action = form.action; console.log(action);
        inputs = $(form)[0].children;
        var data = {};

        for (let i = 0; i < inputs.length; i++) {
          if(inputs[i].name){
            data[inputs[i].name] = inputs[i].value;
          }
        }

        console.log(data);
        
        $.ajax({
          method: "POST",
          url: `${action}`,
          data: data
        }).done( (r) => {
            console.log(r);
            response = JSON.parse(r);
            if(response.error == "0"){
                location.reload();
            }
            alert(response.msg);
        });
        
      }
</script>